#include <SFML/Graphics.hpp>
#include "gameWorld.cpp"
#include "gameTile.cpp"
#include <iostream>


int main() {
    float windowHeight = 400;
    float windowWidth = 400;
    
    sf::RenderWindow window(sf::VideoMode(windowWidth,windowHeight), "SFML works!");

    GameWorld gameWorld = GameWorld();
    int i = 0;

    sf::Texture texture;

    texture.loadFromFile("images/enemy.png");

    sf::Sprite sprite;
    sprite.setTexture(texture);


    sf::Vector2f pos;
    pos = sf::Vector2f(50,50);

    sprite.setPosition(pos);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();

        for (int i = 0; i < gameWorld.gridLength; i++) {
            for (int j = 0; j < gameWorld.gridLength; j++) {
                window.draw(gameWorld.tiles[i][j]->sprite);
            }
        }

        window.draw(sprite);



        
        window.display();
    }

    return 0;
} 