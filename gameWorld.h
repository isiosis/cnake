#include <SFML/Graphics.hpp>
#include "gameTile.h"
#include <vector>

#ifndef GAMEWORLD_H
#define GAMEWORLD_H

class GameWorld {
    void setUpTiles();
    
    public:  
        std::vector< std::vector<GameTile *>> tiles;
        int gridLength;
        GameWorld();
};


#endif