#include <SFML/Graphics.hpp>

#ifndef GAMETILE_H
#define GAMETILE_H

class GameTile {
    public:
        sf::Vector2f pos;
        sf::Sprite sprite;
        sf::Texture texture;
        GameTile(std::string, float, float);
        bool setUpSprite(std::string);
};  

#endif