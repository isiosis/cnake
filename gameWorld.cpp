#include "gameWorld.h"
#include <iostream>

GameWorld::GameWorld() {
    gridLength = 8;
    setUpTiles();
}


void GameWorld::setUpTiles() {
    std::vector<GameTile *> firstRow;
    firstRow.push_back(new GameTile("images/grass.png", 0, 0));
    firstRow.push_back(new GameTile("images/grass.png", 50, 0));
    firstRow.push_back(new GameTile("images/grass.png", 100, 0));
    firstRow.push_back(new GameTile("images/grass.png", 150, 0));
    firstRow.push_back(new GameTile("images/grass.png", 200, 0));
    firstRow.push_back(new GameTile("images/grass.png", 250, 0));
    firstRow.push_back(new GameTile("images/grass.png", 300, 0));
    firstRow.push_back(new GameTile("images/grass.png", 350, 0));
    tiles.push_back(firstRow);

    std::vector<GameTile *> secondRow;
    secondRow.push_back(new GameTile("images/grass.png", 0, 50));
    secondRow.push_back(new GameTile("images/grass.png", 50, 50));
    secondRow.push_back(new GameTile("images/grass.png", 100, 50));
    secondRow.push_back(new GameTile("images/grass.png", 150, 50));
    secondRow.push_back(new GameTile("images/grass.png", 200, 50));
    secondRow.push_back(new GameTile("images/grass.png", 250, 50));
    secondRow.push_back(new GameTile("images/grass.png", 300, 50));
    secondRow.push_back(new GameTile("images/grass.png", 350, 50));
    tiles.push_back(secondRow);

    std::vector<GameTile *> thirdRow;
    thirdRow.push_back(new GameTile("images/grass.png", 0, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 50, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 100, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 150, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 200, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 250, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 300, 100));
    thirdRow.push_back(new GameTile("images/grass.png", 350, 100));
    tiles.push_back(thirdRow);

    std::vector<GameTile *> fourthRow;
    fourthRow.push_back(new GameTile("images/grass.png", 0, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 50, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 100, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 150, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 200, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 250, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 300, 150));
    fourthRow.push_back(new GameTile("images/grass.png", 350, 150));
    tiles.push_back(fourthRow);

    std::vector<GameTile *> fifthRow;
    fifthRow.push_back(new GameTile("images/grass.png", 0, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 50, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 100, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 150, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 200, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 250, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 300, 200));
    fifthRow.push_back(new GameTile("images/grass.png", 350, 200));
    tiles.push_back(fifthRow);

    std::vector<GameTile *> sixthRow;
    sixthRow.push_back(new GameTile("images/grass.png", 0, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 50, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 100, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 150, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 200, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 250, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 300, 250));
    sixthRow.push_back(new GameTile("images/grass.png", 350, 250));
    tiles.push_back(sixthRow);

    std::vector<GameTile *> seventhRow;
    seventhRow.push_back(new GameTile("images/grass.png", 0, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 50, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 100, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 150, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 200, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 250, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 300, 300));
    seventhRow.push_back(new GameTile("images/grass.png", 350, 300));
    tiles.push_back(seventhRow);

    std::vector<GameTile *> eigthRow;
    eigthRow.push_back(new GameTile("images/grass.png", 0, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 50, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 100, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 150, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 200, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 250, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 300, 350));
    eigthRow.push_back(new GameTile("images/grass.png", 350, 350));
    tiles.push_back(eigthRow);
}
