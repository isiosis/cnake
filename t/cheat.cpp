#include<iostream>
#include <SFML/Graphics.hpp>
#include<vector>


using namespace std;
enum eDirection { STOP=0,LEFT,RIGHT,UP,DOWN};
eDirection dira;
bool gameOver;
void setup(); //setup of the structure 
void Draw(); //setup draw the structure
void Input(); //input ask the user for the data
void Logic(); 
const int width=20;
const int height=20;
int x,y, fruitX, fruitY,score;
int tailx[100],taily[100];
int ntail;
vector<sf::Vector2f *> head(100);

int main()
{

	sf::Vector2f vec = sf::Vector2f(5,5);
	sf::Vector2f vec2 = sf::Vector2f(10,10);
	
	sf::Vector2f vec3 = vec + vec2;

	cout << vec3.x << " " << vec3.y;
	
	char ch;
	cout<<"Enter: S\n\n";
	cin>>ch;
	switch (ch)
	{
	case 'S': 
		setup();
		while(!gameOver)
		{
			Draw();
			Input();
			Logic();
			_sleep(10);
		}
		break;
	default:
		cout<<"Invalid!! Please Select Given Options.";
		break;
	}
	
	getch();
	return 0;
}

void setup()
{
	gameOver=false;
	dira=STOP;
	x=width/2;
	y=height/2;
	fruitX=rand()%width;
	fruitY=rand()%height;
	for (int i = 0; i < 100; i++) {
		head[i]->x = 0;
		head[i]->y = 0;
	}
	score=0;
}

void Draw()
{
	system("cls");
	for(int i=0;i<width-8;i++)
	{
		cout<<"||";
	}
	cout<<endl;
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			if(j==0)
			{
				cout<<"\t\t||";
			}
			if(i==y&&j==x)
				cout<<"O";
			else if(i==fruitY&&j==fruitX)
				cout<<"*";
			else
			{
				bool print=false;
				for(int k=0;k<ntail;k++)
				{
					if(head[k]->x ==j&& head[k]->y==i)
					{
						cout<<"o";
						print=true;
					}
				} 
				if(!print)
				{
					cout<<" ";
				}
			}
			if (j==width-1)
				cout<<"||";
		}
		cout<<endl;
	}
	cout<<"\t\t";
	for(int i=0;i<width-8;i++)
	{
		cout<<"||";
	}
	cout<<endl;
	cout<<"\t\t\tScore: "<<score<<endl;
}

void Input()
{
	if(_kbhit())
	{
		switch(_getch())
		{
		case 'a':
			dira=LEFT;
			break;
		case 'd':
			dira=RIGHT;
			break;
		case 'w':
			dira=UP;
			break;
		case 's':
			dira=DOWN;
			break;
		case 'x':
			gameOver=true;
			break;
		}
	}
}
simple mode

void Logic()
{
	int prevX= head[0]->x;
	int prevY = head[0]->y;
	int prev2X,prev2Y;
	head[0]->x = x;
	head[0]->y = y;

	switch (dira)
	{
	case LEFT:
		x--;
		break;
	case RIGHT:
		x++;
		break;
	case UP:
		y--;
		break;
	case DOWN:
		y++;
		break;
	default:
		break;
	}
	if(x>=width) x=0; else if(x<0)x=width-1;
	if(y>=height) y=0; else if(y<0)y=height-1;
	
	for(int i=0;i<ntail;i++)
	{
		if(head[i]->x == x && head[i]->y == y)
		{
			gameOver=true;
		}
	}
	if(x==fruitX&&y==fruitY)
	{
		score+=10;
		fruitX=rand()%width;
		fruitY=rand()%height;
		ntail++;
	}
}

