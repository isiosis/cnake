#include "Snake.h"
#include "SnakeSection.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameWorld.h"

using namespace std;

Snake::Snake(int height, int width) {
    cout << "snake created" << endl;
    sections.push_back(new SnakeSection(int(height/2), int(width/2), "images/snake_head.png"));
	dira = UP;
    size = 0;
	x = int(height/2);
	y = int(width/2);
}

void Snake::logic() {
	cout << "Positions are" << sections[0]->pos.x << ": " << sections[0]->pos.y << endl;
    int prevX = sections[0]->pos.x;
	int prevY =  sections[0]->pos.y;
    int prev2X,prev2Y;
	sections[0]->pos.x = x;
	sections[0]->pos.y = y;
	sections[0]->setPos(sections[0]->pos);

    for(int i=1;i<size;i++)
	{
		prev2X = sections[i]->pos.x;
		prev2Y = sections[i]->pos.y;
		sections[i]->pos.x = prevX;
		sections[i]->pos.y = prevY;
		sections[i]->setPos(sections[0]->pos);
		prevX=prev2X;
		prevY=prev2Y;
	}

    switch (dira)
	{
	case LEFT:
		x -= 1;
		break;
	case RIGHT:
		x += 1;
		break;
	case UP:
		y -= 1;
		break;
	case DOWN:
		y += 1;
		break;
	default:
		break;
	}
}
