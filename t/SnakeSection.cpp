#include "SnakeSection.h"
#include <SFML/Graphics.hpp>

SnakeSection::SnakeSection(int x, int y, std::string tex) {
    texture.loadFromFile(tex);
    pos = sf::Vector2f(x,y);
    sprite.setPosition(pos);
    sprite.setTexture(texture);
    sprite.setScale(5,5);
}   

void  SnakeSection::setPos(sf::Vector2f p) {
    sprite.setPosition(p);
}