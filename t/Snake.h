#include<SFML/Graphics.hpp>
#include<vector>
#include "SnakeSection.h"

#ifndef SNAKE_H
#define SNAKE_H

class Snake {
    public:
        // std::vector<sf::Vector2f *> head;
        // sf::Vector2f fruit;
        
        std::vector<SnakeSection *> sections;
        int x,y, size;
        enum eDirection { STOP=0,LEFT,RIGHT,UP,DOWN};
        eDirection dira = STOP;
        Snake(int height, int width);
        void logic();
};

#endif 