#include <SFML/Graphics.hpp>
#include "GameCell.h"
#include <vector>

#ifndef GAMEWORLD_H
#define GAMEWORLD_H
class GameWorld
{
private:
	sf::Vector2i foodPosition;
	// TODO: snake position ????
	void initializeGround();
	void initializePlayer(); 
public:
	std::vector<std::vector<GameCell*>> cells;
	int gridLength;
	GameWorld();
};

#endif // !GAMEWORLD_H

