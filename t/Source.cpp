#include <SFML/Graphics.hpp>
#include "GameCell.h"
#include "GameWorld.h"
#include "Snake.h"
#include <iostream>
#include<unistd.h>

using namespace std;
/*
void setOriginAndReadjust(sf::Transformable& object, const sf::Vector2f& newOrigin)
{
    auto offset = newOrigin - object.getOrigin();
    object.setOrigin(newOrigin);
    object.move(offset);
}
*/

int main()
{
    float windowHeight = 640;
    float windowWidth = 640;

    sf::RenderWindow window(sf::VideoMode(windowHeight, windowWidth), "Snake Game");
    
    GameWorld gameWorld = GameWorld();

    sf::Texture textureS, textureF;

    
    Snake snake(windowHeight, windowWidth);



    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            snake.dira = snake.UP;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            snake.dira = snake.DOWN;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            snake.dira = snake.LEFT;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            snake.dira = snake.RIGHT;
        }

        cout << snake.sections[0]->sprite.getPosition().x <<  " : " << snake.sections[0]->sprite.getPosition().y;
        cout << endl;
        snake.logic();
        

        window.clear();

        // for (int i = 0; i < gameWorld.gridLength; i++) {
        //     for (int j = 0; j < gameWorld.gridLength; j++) {
        //         window.draw(gameWorld.cells[i][j]->sprite);
        //     }
        // }

        for (int i = 0; i <= snake.size; i++) {
         window.draw(snake.sections[i]->sprite);
        }
        window.display();
        usleep(350000);
    }

    return 0;
}