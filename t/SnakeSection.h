#include<vector>
#include<iostream>
#include<SFML/Graphics.hpp>

#ifndef SNAKESECTION_H
#define SNAKESECTION_H

class SnakeSection {
    public:
        sf::Vector2f pos;
        sf::Texture texture;
        sf::Sprite sprite;

        SnakeSection(int x, int y, std::string texture);
        void setPos(sf::Vector2f p);
};

#endif